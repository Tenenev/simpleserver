﻿using System;

namespace SimpleServer.BL.Attributes
{
    /// <summary>
    /// Attribute for property mapping from the request
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class RequestMapAttribute : Attribute
    {
        public string MapTo { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestMapAttribute"/> attribute.
        /// </summary>
        /// <param name="mapTo">The name of the property for mapping.</param>
        public RequestMapAttribute(string mapTo = null)
        {
            MapTo = mapTo;
        }
    }
}
