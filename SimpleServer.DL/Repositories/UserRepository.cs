﻿using System;
using System.Linq;
using SimpleServer.BL.Common;
using SimpleServer.BL.Models;
using SimpleServer.BL.Repositories;

namespace SimpleServer.DL.Repositories
{
    public class UserRepository : BaseRepository<UserModel>, IUserRepository
    {
        public UserRepository(IRepositoryContext context) : base(context) { }

        /// <summary>
        /// Gets the user by name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public UserModel GetByName(string name)
        {
            return GetAll().FirstOrDefault(u => u.Name == name);
        }
    }
}
