﻿using System;
using System.Configuration;
using SimpleServer.BL.Common;
using SimpleServer.DL.Enums;
using SimpleServer.DL.Helpers;
using SimpleServer.Server.Common;

namespace SimpleServer.Server
{
    class Program
    {
        private const int DefaultTcpPort = 8080;

        static void Main(string[] args)
        {
            StorageType storageType;
            int? tcpPort = null;

            try
            {
                // Gets the storage type and port from configuration file
                storageType = StorageHelper.ParseStorageType(ConfigurationManager.AppSettings["StorageType"]);
                tcpPort = int.Parse(ConfigurationManager.AppSettings["ServerTcpPort"]);
            }
            catch (Exception)
            {
                // Set defaults
                storageType = StorageType.XML;
                tcpPort = DefaultTcpPort;
            }

            // Initialize the dependencies
            var container = new DependencyContainer(storageType);

            // Launch the server
            var server = new SimpleHttpServer(tcpPort.Value, new RouteProvider(container.GetContainer(), new ConsoleLogger()));
        }
    }
}
