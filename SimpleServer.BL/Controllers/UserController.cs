﻿using System;
using System.Collections.Generic;
using SimpleServer.BL.Enums;
using SimpleServer.BL.Repositories;

namespace SimpleServer.BL.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public override string Display(Dictionary<string, string> parameters, HttpRequestType requestType)
        {
            return "Hello, world!";
        }
    }
}