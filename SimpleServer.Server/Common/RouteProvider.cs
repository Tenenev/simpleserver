﻿using System;
using Autofac;
using SimpleServer.BL.Common;
using SimpleServer.BL.Controllers;

namespace SimpleServer.Server.Common
{
    public class RouteProvider : RouteProviderBase
    {
        private readonly IContainer _container;

        public RouteProvider(IContainer container, ILogger logger)
            : base(logger)
        {
            _container = container;
        }

        /// <summary>
        /// Gets the controller.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        protected override BaseController GetObject(string type)
        {
            return _container.ResolveNamed<BaseController>(type);
        }
    }
}
