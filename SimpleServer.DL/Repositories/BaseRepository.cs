﻿using System;
using System.Linq;
using SimpleServer.BL.Common;
using SimpleServer.BL.Models;
using SimpleServer.BL.Repositories;

namespace SimpleServer.DL.Repositories
{
    public abstract class BaseRepository
    {
        
    }

    public abstract class BaseRepository<T> : BaseRepository, IRepository<T> where T : BaseModel
    {
        protected readonly IRepositoryContext Context;

        public BaseRepository(IRepositoryContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Select all entities from the store.
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> GetAll()
        {
            return Context.Load<T>();
        }

        /// <summary>
        /// Gets the entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public T GetById(Guid id)
        {
            return GetAll().FirstOrDefault(new Func<T, bool>(m => m.Id == id));
        }

        /// <summary>
        /// Adds entity to the store.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Add(T entity)
        {
            entity.Id = Guid.NewGuid();
            Context.Add(entity);
            Context.SaveChanges<T>();
        }

        /// <summary>
        /// Deletes entity from the store.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void Delete(T entity)
        {
            throw new NotImplementedException();
        }
    }
}