﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using SimpleServer.BL.Common;
using SimpleServer.BL.Enums;
using SimpleServer.BL.Helpers;

namespace SimpleServer.Server.Common
{
    public class SimpleTcpClient
    {
        private static readonly Regex RequestRegex = new Regex(@"^\w+\s+([^\s\?]+)([^\s]*)\s+HTTP/.*|");
        private static readonly Regex ProxyRegex = new Regex(@"^(\w+\s+)\/proxy\?url=([^\s]+)");

        private string _request = string.Empty;

        private readonly RouteProviderBase _provider;
        private readonly TcpClient _client;

        public SimpleTcpClient(TcpClient client, RouteProviderBase provider)
        {
            _provider = provider;
            _client = client;

            var buffer = new byte[1024];
            int count;

            while ((count = _client.GetStream().Read(buffer, 0, buffer.Length)) > 0)
            {
                _request += Encoding.ASCII.GetString(buffer, 0, count);

                if (_request.IndexOf("\r\n\r\n", StringComparison.Ordinal) >= 0)
                {
                    break;
                }
            }

            SendResponse();
        }

        /// <summary>
        /// Sends the response depends on request type.
        /// </summary>
        private void SendResponse()
        {
            HandleProxy();

            var requestType = HttpRequestHelper.GetRequestType(_request);

            if (requestType == HttpRequestType.GET)
            {
                HandleGetRequest();
            }
            else if (requestType == HttpRequestType.POST)
            {
                HandlePostRequest();
            }
            else
            {
                SendError(500);
                return;
            }
        }

        /// <summary>
        /// Handles the proxy request.
        /// </summary>
        private void HandleProxy()
        {
            var proxyRegexMatch = ProxyRegex.Match(_request);
            if (proxyRegexMatch.Success)
            {
                _request = ProxyRegex.Replace(_request, proxyRegexMatch.Groups[1].Value + proxyRegexMatch.Groups[2].Value);
            }
        }

        /// <summary>
        /// Handles the GET request.
        /// </summary>
        private void HandleGetRequest()
        {
            var reqMatch = RequestRegex.Match(_request);

            if (!reqMatch.Success)
            {
                SendError(400);
                return;
            }

            var requestUri = reqMatch.Groups[1].Value;
            var requestParameters = reqMatch.Groups[2].Value;

            requestUri = Uri.UnescapeDataString(requestUri);
            requestParameters = Uri.UnescapeDataString(requestParameters);

            if (requestUri.IndexOf("..", StringComparison.Ordinal) >= 0)
            {
                SendError(400);
                return;
            }

            // Parsing request
            var parameters = HttpRequestHelper.ParseGetRequest(requestParameters);

            int? errorCode;
            var responseHtml = _provider.GetResponse(requestUri, parameters, HttpRequestType.GET, out errorCode);

            if (errorCode.HasValue)
            {
                SendError(errorCode.Value);
                return;
            }

            var html = string.Format("<html><body>{0}</body></html>", responseHtml);
            var str = "HTTP/1.1 200 OK\nContent-type: text/html\nContent-Length:" + html.Length + "\n\n" + html;
            var buffer = Encoding.ASCII.GetBytes(str);
            _client.GetStream().Write(buffer, 0, buffer.Length);
            _client.Close();
        }

        /// <summary>
        /// Handles the POST request.
        /// </summary>
        private void HandlePostRequest()
        {
            var reqMatch = RequestRegex.Match(_request);

            if (!reqMatch.Success)
            {
                SendError(400);
                return;
            }

            var requestUri = reqMatch.Groups[1].Value;

            requestUri = Uri.UnescapeDataString(requestUri);

            if (requestUri.IndexOf("..", StringComparison.Ordinal) >= 0)
            {
                SendError(400);
                return;
            }

            // Parsing request
            var parameters = HttpRequestHelper.ParsePostRequest(_request);

            int? errorCode;
            var responseHtml = _provider.GetResponse(requestUri, parameters, HttpRequestType.POST, out errorCode);

            if (errorCode.HasValue)
            {
                SendError(errorCode.Value);
                return;
            }

            var html = string.Format("<html><body>{0}</body></html>", responseHtml);
            var str = "HTTP/1.1 200 OK\nContent-type: text/html\nContent-Length:" + html.Length + "\n\n" + html;
            var buffer = Encoding.ASCII.GetBytes(str);
            _client.GetStream().Write(buffer, 0, buffer.Length);
            _client.Close();
        }

        /// <summary>
        /// Sends the error with selected code.
        /// </summary>
        /// <param name="code">The code.</param>
        private void SendError(int code)
        {
            var codeStr = code + " " + ((HttpStatusCode)code);
            var html = "<html><body>" + codeStr + "</body></html>";
            var str = "HTTP/1.1 " + codeStr + "\nContent-type: text/html\nContent-Length:" + html.Length + "\n\n" + html;
            var buffer = Encoding.ASCII.GetBytes(str);
            _client.GetStream().Write(buffer, 0, buffer.Length);
            _client.Close();
        }
    }
}