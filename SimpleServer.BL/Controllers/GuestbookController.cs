﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleServer.BL.Enums;
using SimpleServer.BL.Helpers;
using SimpleServer.BL.Models;
using SimpleServer.BL.Repositories;

namespace SimpleServer.BL.Controllers
{
    public class GuestbookController : BaseController
    {
        private readonly IMessageRepository _messageRepository;
        private readonly IUserRepository _userRepository;

        public GuestbookController(IMessageRepository messageRepository, IUserRepository userRepository)
        {
            _messageRepository = messageRepository;
            _userRepository = userRepository;
        }

        public override string Display(Dictionary<string, string> parameters, HttpRequestType requestType)
        {
            // Display different pages depends on request type
            switch (requestType)
            {
                case HttpRequestType.GET:
                    return DisplayGet(parameters);
                case HttpRequestType.POST:
                    return DisplayPost(parameters);
            }
            return "Hello, world!";
        }

        private string DisplayGet(Dictionary<string, string> parameters)
        {
            var messages = _messageRepository.GetAll().ToList();

            var output = new StringBuilder();

            output.Append("<ul>");

            foreach (var message in messages)
            {
                output.Append("<li>");

                output.Append(string.Format("{0}: {1}", message.User, message.Content));

                output.Append("</li>");
            }

            output.Append("</ul>");

            return output.ToString();
        }

        private string DisplayPost(Dictionary<string, string> parameters)
        {
            var message = RequestMapper.Map<MessageModel>(parameters);

            _messageRepository.Add(message);

            var user = _userRepository.GetByName(message.User);

            if (user == null)
            {
                user = new UserModel {Name = message.User};
                _userRepository.Add(user);
            }

            return "Hello, world!";
        }
    }
}