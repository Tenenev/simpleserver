﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleServer.BL.Models;

namespace SimpleServer.DL.SQLite.Common
{
    public class SQLiteDataContext : DbContext
    {
        public DbSet<MessageModel> Messages { get; set; }

        public DbSet<UserModel> Users { get; set; }
    }
}
