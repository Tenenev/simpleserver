﻿using System.Collections.Generic;
using SimpleServer.BL.Enums;

namespace SimpleServer.BL.Controllers
{
    public abstract class BaseController
    {
        public abstract string Display(Dictionary<string, string> parameters, HttpRequestType requestType);
    }
}