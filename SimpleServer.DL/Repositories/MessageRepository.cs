﻿using SimpleServer.BL.Common;
using SimpleServer.BL.Models;
using SimpleServer.BL.Repositories;

namespace SimpleServer.DL.Repositories
{
    public class MessageRepository : BaseRepository<MessageModel>, IMessageRepository
    {
        public MessageRepository(IRepositoryContext context) : base(context){}
    }
}
