﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using SimpleServer.BL.Attributes;

namespace SimpleServer.BL.Helpers
{
    public static class RequestMapper
    {
        public static T Map<T>(Dictionary<string, string> parameters)
        {
            // Get all fields for mapping the request
            var fieldsToMap =
                typeof (T).GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty)
                    .Where(p => p.GetCustomAttribute<RequestMapAttribute>() != null)
                    .ToList();

            // Create the instance
            var result = Activator.CreateInstance<T>();

            foreach (var propertyInfo in fieldsToMap)
            {
                var mapAttribute = propertyInfo.GetCustomAttribute<RequestMapAttribute>();

                // If "MapTo" property is not set take the original property name
                var mappingProperty = (mapAttribute.MapTo ?? propertyInfo.Name).ToLowerInvariant();

                var key = parameters.Keys.FirstOrDefault(k => k.ToLowerInvariant() == mappingProperty);
                if (key == null) continue;
                propertyInfo.SetValue(result, parameters[key]);
            }

            return result;
        }
    }
}
