﻿using System;
using SimpleServer.DL.Xml.Helpers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using SimpleServer.BL.Models;

namespace SimpleServer.DL.Xml.Common
{
    /// <summary>
    /// The universal XML storage for different types of entities
    /// </summary>
    public class XmlStore
    {
        private const string RootNodeName = "XmlStore";

        private XmlDocument _document;
        private XmlElement _rootElement;

        public void Load(string filename)
        {
            _document = new XmlDocument();
            if (File.Exists(filename))
            {
                _document.Load(filename);
                _rootElement = (XmlElement)_document.SelectSingleNode(string.Format(@"/{0}", RootNodeName));
            }
            else
            {
                var xmlDeclaration = _document.CreateXmlDeclaration("1.0", "UTF-8", null);
                var root = _document.DocumentElement;
                _document.InsertBefore(xmlDeclaration, root);
                _rootElement = _document.CreateElement(RootNodeName);
                _document.AppendChild(_rootElement);
                Save(filename);
            }
        }

        public void Save(string filename)
        {
            _document.Save(filename);
        }

        public bool ContainsType<T>() where T : BaseModel
        {
            return _document.SelectSingleNode(string.Format("/{0}/{1}Collection", RootNodeName, typeof(T).Name)) != null;
        }

        public List<T> GetCollection<T>() where T: BaseModel
        {
            var nodes = _document.SelectNodes(string.Format("/{0}/{1}Collection/{1}", RootNodeName, typeof(T).Name));

            return (from XmlElement node in nodes select SerializeHelper.DeserializeModel<T>(node)).ToList();
        }

        public void SetCollection<T>(IEnumerable<T> models) where T : BaseModel
        {
            var containerNode = _document.SelectSingleNode(string.Format("/{0}/{1}Collection", RootNodeName, typeof(T).Name));
            if (containerNode == null)
            {
                containerNode = _document.CreateElement(string.Format("{0}Collection", typeof(T).Name));
                _rootElement.AppendChild(containerNode);
            }
            else
            {
                containerNode.RemoveAll();
            }

            foreach (var model in models)
            {
                var node = _document.CreateElement(typeof (T).Name);
                node.InnerXml = SerializeHelper.SerializeModel(model).InnerXml;
                containerNode.AppendChild(node);
            }
        }
    }
}