﻿using System;
using System.Linq;
using SimpleServer.BL.Models;

namespace SimpleServer.BL.Common
{
    public interface IRepositoryContext : IDisposable
    {
        /// <summary>
        /// Loads all entities of target type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IQueryable<T> Load<T>() where T : BaseModel;

        /// <summary>
        /// Adds the entity to store.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model">The model.</param>
        void Add<T>(T model) where T : BaseModel;

        /// <summary>
        /// Saves the changes.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        void SaveChanges<T>() where T : BaseModel;
    }
}