﻿using System;
using SimpleServer.BL.Common;

namespace SimpleServer.Server.Common
{
    public class ConsoleLogger : ILogger
    {
        public void Log(string str)
        {
            Console.WriteLine(str);
        }
    }
}
