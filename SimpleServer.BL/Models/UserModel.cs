﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleServer.BL.Common;

namespace SimpleServer.BL.Models
{
    public class UserModel : BaseModel
    {
        public string Name { get; set; }
    }
}
