﻿using System;
using SimpleServer.BL.Attributes;

namespace SimpleServer.BL.Models
{
    public class MessageModel : BaseModel
    {
        [RequestMap]
        public string User { get; set; }

        [RequestMap("Message")]
        public string Content { get; set; }
    }
}