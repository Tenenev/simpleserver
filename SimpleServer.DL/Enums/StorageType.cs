﻿namespace SimpleServer.DL.Enums
{
    public enum StorageType
    {
        XML,
        SQLite
    }
}