﻿namespace SimpleServer.BL.Common
{
    public interface ILogger
    {
        void Log(string str);
    }
}
