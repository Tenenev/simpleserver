﻿using System;
using System.Linq;
using Autofac;
using SimpleServer.BL.Common;
using SimpleServer.BL.Controllers;
using SimpleServer.BL.Repositories;
using SimpleServer.DL.Enums;
using SimpleServer.DL.Repositories;
using SimpleServer.DL.SQLite.Common;
using SimpleServer.DL.Xml.Common;

namespace SimpleServer.Server.Common
{
    class DependencyContainer
    {
        private readonly IContainer _container;
        private readonly StorageType _storageType;

        public DependencyContainer(StorageType storageType)
        {
            _storageType = storageType;

            var builder = new ContainerBuilder();

            RegisterDependencies(builder);

            _container = builder.Build();
        }

        public IContainer GetContainer()
        {
            return _container;
        }

        private void RegisterDependencies(ContainerBuilder builder)
        {
            switch (_storageType)
            {
                case StorageType.XML:
                    builder.RegisterType<XmlRepositoryContext>().As<IRepositoryContext>();
                    break;
                case StorageType.SQLite:
                    builder.RegisterType<SQLiteRepositoryContext>().As<IRepositoryContext>();
                    break;
            }

            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<MessageRepository>().As<IMessageRepository>();

            // Register all controllers
            var controllerTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(
                    c =>
                        typeof (BaseController).IsAssignableFrom(c) && c != typeof (BaseController) &&
                        c.Name.EndsWith("Controller")).ToList();
            foreach (var controllerType in controllerTypes)
            {
                builder.RegisterType(controllerType).Keyed<BaseController>(controllerType.Name);
            }
        }
    }
}
