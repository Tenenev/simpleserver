﻿using System;

namespace SimpleServer.BL.Models
{
    public abstract class BaseModel
    {
        public Guid Id { get; set; }
    }
}
