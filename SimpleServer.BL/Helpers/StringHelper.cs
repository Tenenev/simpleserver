﻿namespace SimpleServer.BL.Helpers
{
    public static class StringHelper
    {
        /// <summary>
        /// To the cammel case.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static string ToUpperFirst(this string str)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(str[0]) + str.Substring(1).ToLowerInvariant();
        }
    }
}
