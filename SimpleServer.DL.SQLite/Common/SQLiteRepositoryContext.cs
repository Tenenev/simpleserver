﻿using System;
using System.Collections.Generic;
using System.Linq;
using SimpleServer.BL.Common;
using SimpleServer.BL.Models;

namespace SimpleServer.DL.SQLite.Common
{
    public class SQLiteRepositoryContext : IRepositoryContext
    {
        private readonly SQLiteDataContext _db;

        public SQLiteRepositoryContext()
        {
            _db = new SQLiteDataContext();
        }

        public IQueryable<T> Load<T>() where T : BaseModel
        {
            return _db.Set<T>();
        }

        public void Add<T>(T model) where T : BaseModel
        {
            _db.Set<T>().Add(model);
        }

        public void SaveChanges<T>() where T : BaseModel
        {
            _db.SaveChanges();
        }

        public void Dispose()
        {
            if (_db != null)
                _db.Dispose();
        }
    }
}
