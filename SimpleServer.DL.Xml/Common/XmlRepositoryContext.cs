﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using SimpleServer.BL.Common;
using SimpleServer.BL.Models;

namespace SimpleServer.DL.Xml.Common
{
    public class XmlRepositoryContext : IRepositoryContext
    {
        private const string FileName = "XmlStore.xml";

        private static readonly string FilePath;

        static XmlRepositoryContext()
        {
            FilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? string.Empty,
                FileName);
        }

        private readonly Dictionary<Type, List<object>> _contextCache;

        public XmlRepositoryContext()
        {
            _contextCache = new Dictionary<Type, List<object>>();
        }

        public IQueryable<T> Load<T>() where T : BaseModel
        {
            var store = new XmlStore();
            store.Load(FilePath);

            var collection = store.GetCollection<T>();

            if (!_contextCache.ContainsKey(typeof (T)))
            {
                _contextCache.Add(typeof(T), new List<object>(collection));
            }
            else
            {
                _contextCache[typeof(T)] = new List<object>(collection);
            }

            return collection.AsQueryable();
        }

        public void Add<T>(T model) where T : BaseModel
        {
            Load<T>();

            var collection = _contextCache[typeof (T)];

            collection.Add(model);
        }

        public void SaveChanges<T>() where T : BaseModel
        {
            if (!_contextCache.ContainsKey(typeof (T))) return;

            var collection = _contextCache[typeof (T)].Cast<T>().ToList();

            var store = new XmlStore();
            store.Load(FilePath);
            store.SetCollection(collection);
            store.Save(FilePath);
        }

        public void Dispose()
        {
            _contextCache.Clear();
        }
    }
}