﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SimpleServer.BL.Controllers;
using SimpleServer.BL.Enums;
using SimpleServer.BL.Helpers;

namespace SimpleServer.BL.Common
{
    public abstract class RouteProviderBase
    {
        private readonly ILogger _logger;

        // Collection of the registered controllers
        private readonly List<string> _controllerNames;

        // Regex for recognize controller name
        private static readonly Regex RequestControllerRegex = new Regex(@"^\/([A-z\d]+)(?:\/(.*))?$");

        public RouteProviderBase(ILogger logger)
        {
            _logger = logger;

            // Get the collection of registered controllers
            var controllerFullNames = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(c => typeof (BaseController).IsAssignableFrom(c) && c != typeof(BaseController) && c.Name.EndsWith("Controller")).Select(c => c.Name).ToList();

            _controllerNames = controllerFullNames.ToList();
        }

        /// <summary>
        /// Gets the response.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="requestType">Type of the request (GET/PUT/POST/DELETE).</param>
        /// <param name="errorCode">The error code.</param>
        /// <returns></returns>
        public string GetResponse(string request, Dictionary<string, string> parameters, HttpRequestType requestType, out int? errorCode)
        {
            errorCode = null;

            // Log request info
            _logger.Log(string.Format("Request type: {0}, request: {1}, parameters: {2}", requestType, request,
                string.Join(", ", parameters.Select(p => string.Format("{0}: {1}", p.Key, p.Value)))));

            // Try to match the controller name
            var match = RequestControllerRegex.Match(request);

            if (!match.Success)
            {
                // Controller name is not valid
                errorCode = 404;
                return null;
            }

            // Gets the full controller name
            var controllerName = match.Groups[1].Value.ToUpperFirst() + "Controller";

            // Try to find controller name in registered controllers collection
            if (!_controllerNames.Contains(controllerName))
            {
                errorCode = 404;
                return null;
            }

            // Get the parameters for action
            var actionRequest = match.Groups[2].Value;

            // Gets the controller
            var controller = GetObject(controllerName);

            if (controller == null)
            {
                errorCode = 500;
                return null;
            }

            // Displaying the action
            return controller.Display(parameters, requestType);
        }

        /// <summary>
        /// Gets the controller.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        protected abstract BaseController GetObject(string type);
    }
}
