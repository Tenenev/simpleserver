﻿using System;
using System.Linq;
using SimpleServer.BL.Models;

namespace SimpleServer.BL.Repositories
{
    public interface IRepository
    {
        
    }

    public interface IRepository<T> : IRepository where T : BaseModel
    {
        /// <summary>
        /// Select all entities from the store.
        /// </summary>
        /// <returns></returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Gets the entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        T GetById(Guid id);

        /// <summary>
        /// Adds entity to the store.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void Add(T entity);

        /// <summary>
        /// Deletes entity from the store.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void Delete(T entity);
    }
}
