﻿using SimpleServer.BL.Common;
using SimpleServer.BL.Models;

namespace SimpleServer.BL.Repositories
{
    public interface IMessageRepository : IRepository<MessageModel>
    {
    }
}
