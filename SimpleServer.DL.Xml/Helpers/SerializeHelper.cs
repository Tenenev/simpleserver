﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using SimpleServer.BL.Common;
using System.Xml;
using SimpleServer.BL.Models;

namespace SimpleServer.DL.Xml.Helpers
{
    public static class SerializeHelper
    {
        /// <summary>
        /// Serializes the model.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public static XmlElement SerializeModel<T>(T model) where T : BaseModel
        {
            string nodeStr = null;
            var serializer = new XmlSerializer(typeof (T));
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, model);
                nodeStr = writer.ToString();
            }

            var doc = new XmlDocument();
            doc.LoadXml(nodeStr);
            return doc.DocumentElement;
        }

        /// <summary>
        /// Deserializes the model.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        public static T DeserializeModel<T>(XmlElement element) where T : BaseModel
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var reader = new StringReader(element.OuterXml))
            {
                return serializer.Deserialize(reader) as T;
            }
        }
    }
}
