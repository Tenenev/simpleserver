﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using SimpleServer.BL.Common;

namespace SimpleServer.Server.Common
{
    public class SimpleHttpServer
    {
        private readonly TcpListener _listener;
        private readonly RouteProviderBase _provider;

        public SimpleHttpServer(int port, RouteProviderBase provider)
        {
            _provider = provider;
            _listener = new TcpListener(IPAddress.Any, port);
            _listener.Start();

            while (true)
            {
                // Accept the new client
                var client = _listener.AcceptTcpClient();
                // Chreate new thread
                var thread = new Thread(ClientThread);
                // Launch new thread with the client
                thread.Start(client);
            }
        }

        private void ClientThread(Object stateInfo)
        {
            new SimpleTcpClient((TcpClient)stateInfo, _provider);
        }

        ~SimpleHttpServer()
        {
            if (_listener != null)
            {
                _listener.Stop();
            }
        }
    }
}
