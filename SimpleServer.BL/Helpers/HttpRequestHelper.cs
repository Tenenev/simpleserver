﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SimpleServer.BL.Enums;

namespace SimpleServer.BL.Helpers
{
    public static class HttpRequestHelper
    {
        private static readonly Regex RequestTypeRegex = new Regex(@"^(\w+)\s");

        private static readonly Regex ParseGetRegex = new Regex(@"\?([^\?]+)$");

        private static readonly Regex ParsePostRegex = new Regex(@"\s(\S+)$");

        // Regex for parsing key-value pair of parameters
        private static readonly Regex ParseParameterRegex = new Regex(@"^([^=]+)=([^=]+)$");

        /// <summary>
        /// Recognizing the request type.
        /// </summary>
        /// <param name="requestStr">The request string.</param>
        /// <returns></returns>
        public static HttpRequestType GetRequestType(string requestStr)
        {
            var match = RequestTypeRegex.Match(requestStr);

            if(!match.Success) return HttpRequestType.Unknown;

            return (HttpRequestType) Enum.Parse(typeof (HttpRequestType), match.Groups[1].Value, false);
        }

        /// <summary>
        /// Gets parameters from GET request.
        /// </summary>
        /// <param name="requestStr">The request string.</param>
        /// <returns></returns>
        public static Dictionary<string, string> ParseGetRequest(string requestStr)
        {
            return ParseRequest(requestStr, ParseGetRegex);
        }

        /// <summary>
        ///  Gets parameters from POST request.
        /// </summary>
        /// <param name="requestStr">The request string.</param>
        /// <returns></returns>
        public static Dictionary<string, string> ParsePostRequest(string requestStr)
        {
            return ParseRequest(requestStr, ParsePostRegex);
        }

        /// <summary>
        /// Gets parameters from request.
        /// </summary>
        /// <param name="requestStr">The request string.</param>
        /// <param name="regex">The regex.</param>
        /// <returns></returns>
        private static Dictionary<string, string> ParseRequest(string requestStr, Regex regex)
        {
            var request = new Dictionary<string, string>();

            var match = regex.Match(requestStr);

            if (!match.Success) return request;

            var requestQuery = match.Groups[1].Value;

            foreach (var parameter in requestQuery.Split('&'))
            {
                var parameterMatch = ParseParameterRegex.Match(parameter);
                if (!parameterMatch.Success) continue;
                request.Add(Uri.UnescapeDataString(parameterMatch.Groups[1].Value), Uri.UnescapeDataString(parameterMatch.Groups[2].Value));
            }

            return request;
        }
    }
}
