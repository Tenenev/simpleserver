﻿namespace SimpleServer.BL.Enums
{
    public enum HttpRequestType
    {
        Unknown,
        GET,
        PUT,
        POST,
        DELETE
    }
}
