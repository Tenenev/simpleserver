﻿using System;
using SimpleServer.DL.Enums;

namespace SimpleServer.DL.Helpers
{
    public static class StorageHelper
    {
        /// <summary>
        /// Parses the type of the storage.
        /// </summary>
        /// <param name="storageTypeStr">The storage type string.</param>
        /// <returns></returns>
        public static StorageType ParseStorageType(string storageTypeStr)
        {
            if (storageTypeStr == null) return default(StorageType);

            return (StorageType) Enum.Parse(typeof (StorageType), storageTypeStr, true);
        }
    }
}