﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using SimpleServer.BL.Models;
using SimpleServer.DL.Xml.Common;
using SimpleServer.DL.Xml.Helpers;

namespace SimpleServer.UnitTests
{
    [TestFixture]
    public class XmlTests
    {
        private readonly string _assemblyLocation;
        private readonly string _filePath;

        private const string FileName = "XmlStore.xml";

        public XmlTests()
        {
            _assemblyLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            _filePath = Path.Combine(_assemblyLocation, FileName);
        }

        [SetUp]
        public void FixtureSetUp()
        {
            if (File.Exists(_filePath))
                File.Delete(_filePath);
        }

        [Test]
        public void ContainsTypeTest()
        {
            var store = new XmlStore();

            store.Load(_filePath);

            var containsType = store.ContainsType<MessageModel>();

            Assert.IsFalse(containsType);
        }

        [Test]
        public void AddGetMessagesTest()
        {
            var messages = GetMessages();

            var store = new XmlStore();
            store.Load(_filePath);
            store.SetCollection(messages);
            store.Save(_filePath);

            var newMessages = store.GetCollection<MessageModel>();

            Assert.AreEqual(newMessages.Count, 2);
            Assert.AreEqual(messages.Count, newMessages.Count);
            Assert.AreEqual(newMessages.OrderBy(m => m.User).First().User, "Bob Johnson");
            Assert.AreEqual(newMessages.OrderBy(m => m.User).Last().Content, "Hello, world!");
        }

        [Test]
        public void AddMoreTypes()
        {
            var messages = GetMessages();
            var users = GetUsers();

            var store = new XmlStore();
            store.Load(_filePath);
            store.SetCollection(messages);
            store.SetCollection(users);
            store.Save(_filePath);

            var newMessages = store.GetCollection<MessageModel>();
            var newUsers = store.GetCollection<UserModel>();

            Assert.AreEqual(newMessages.Count, 2);
            Assert.AreEqual(messages.Count, newMessages.Count);
            Assert.AreEqual(newMessages.OrderBy(m => m.User).First().User, "Bob Johnson");
            Assert.AreEqual(newMessages.OrderBy(m => m.User).Last().Content, "Hello, world!");

            Assert.AreEqual(newUsers.Count, 3);
            Assert.AreEqual(users.Count, newUsers.Count);
            Assert.AreEqual(newUsers.OrderBy(m => m.Name).First().Name, "Bob Johnson");
            Assert.AreEqual(newUsers.OrderBy(m => m.Name).Last().Name, "Mike Tenenev");
        }

        #region Private methods

        private List<MessageModel> GetMessages()
        {
            return new List<MessageModel>
                {
                    new MessageModel {Id = Guid.NewGuid(), User = "Mike Tenenev", Content = "Hello, world!"},
                    new MessageModel {Id = Guid.NewGuid(), User = "Bob Johnson", Content = "Hello, dude"}
                };
        }

        private List<UserModel> GetUsers()
        {
            return new List<UserModel>
            {
                new UserModel {Id = Guid.NewGuid(), Name = "Mike Tenenev"},
                new UserModel {Id = Guid.NewGuid(), Name = "Bob Johnson"},
                new UserModel {Id = Guid.NewGuid(), Name = "David McDigger"}
            };
        }

        #endregion

        [TearDown]
        public void FixtureTearDown()
        {
        }
    }
}
