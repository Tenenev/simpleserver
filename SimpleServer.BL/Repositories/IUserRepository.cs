﻿using SimpleServer.BL.Models;

namespace SimpleServer.BL.Repositories
{
    public interface IUserRepository : IRepository<UserModel>
    {
        /// <summary>
        /// Gets the user by name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        UserModel GetByName(string name);
    }
}
